﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace контр4
{
    public partial class Form2 : Form
    {
        public string x1
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }
        public string x2
        {
            get { return textBox2.Text; }
            set { textBox2.Text = value; }
        }
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text + ":" + textBox2.Text;
            Form1 frm1 = this.Owner as Form1;
            if (frm1 != null)
            {
                frm1.RangeLable = s;
                frm1.RangeList += s;
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
