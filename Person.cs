﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kontr3
{
    class Person
    {
        private String name
        {
            get {return name;}
            set {name = value;}
        }
        private String lastname
        {
            get;
            set;
        }
        private DateTime birthday
        {
            get { return birthday; }
            set { birthday = value; }
        }
        public Person(String name, String lastname, DateTime birthday)
        {
            this.name = name;
            this.lastname = lastname;
            this.birthday = birthday;
        }

    }
}
